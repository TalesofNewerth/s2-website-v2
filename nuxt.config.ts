import fs from "fs";
import https from 'https';
import { NuxtConfig } from '@nuxt/types';

// const https = require('https');
const config: NuxtConfig = {
    buildModules: ["@nuxtjs/vuetify", "@nuxt/typescript-build"],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        'cookie-universal-nuxt',
    ],
    build: {
        loaders: {
            scss: {
                implementation: require("sass"),
            },
        },
        postcss: false,
        transpile: [
            "vee-validate/dist/rules"
        ]
    },
    srcDir: "src/",
    head: {
        titleTemplate: "Savage 2",
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial scale=1" },
            {
                name: "keywords",
                content:
                    "Savage 2, Savage 2 A Tortured Soul, A Tortured Soul, Savage2, Savage, Savage1, Free, Multiplayer, Strategy, Games, Video games, SavageII",
            },
            {
                name: "description",
                content:
                    "Savage 2: A Tortured Soul is a fantasy and science fiction themed video game that combines elements of the first-person shooter, real-time strategy, and action role-playing game genres.",
            },
        ],
    },
    css: ["@mdi/font/css/materialdesignicons.css", "src/styles/styles.scss"],
    plugins: [
        "~/plugins/event-bus.ts",
        '~/plugins/global-components.ts',
        '~/plugins/auth-service.ts',
        '~/plugins/axios.js',
        '~/plugins/vee-validate.ts',
        { src: '~/plugins/apex-charts.ts', mode: 'client' },
    ],
    filters: [],
    vuetify: {
        theme: {
            dark: true,
            themes: {
                light: {
                    primary: '#ffa500',
                    accent: '#1e81b0',
                    background: '#0c0c0c'
                },
                dark: {
                    primary: '#ffa500',
                    accent: '#1e81b0',
                    background: '#0c0c0c'
                }
            }
        }
    },
    ssr: true,
    server: process.env.NODE_ENV !== 'production' ? {
        https: {
            key: fs.readFileSync('./certs/localhost+2-key.pem'),
            cert: fs.readFileSync('./certs/localhost+2.pem')
        }
    } : {},
    axios: {
        proxy: true,
    },
    generate: {
        dir: 'docker/dist'
    },
    proxy: {
        '/api/': {
            target: 'http://s2api',
            logLevel: "debug",
            // target: 'https://localhost:5001',
            changeOrigin: true,
            secure: false
        }
    }
};

export default config;
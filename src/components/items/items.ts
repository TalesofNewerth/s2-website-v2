import {ref, Ref} from 'vue';
import {AxiosResponse} from 'axios';
import {Item} from "@/models/Items/Item";
import {RuneStageEnum} from "@/enums/items/RuneStageEnum";
import {NuxtAxiosInstance} from "@nuxtjs/axios";

let cachedData: Item[] | null = null;
let isFetching: boolean = false;
const types: Ref<Item[]> = ref([]);
const regens: Ref<Item[]> = ref([]);
const passives: Ref<Item[]> = ref([]);
const actives: Ref<Item[]> = ref([]);
const imageBasePath: string = "/img/icons/rune-icons/";
const runeGoldCost: number = 100;
const salvagedGoldCost: number = runeGoldCost * 0.4;
const randomGoldCost: number = runeGoldCost * 0.5;
export function useItems() {
    function getPassive(value: number): Item {
        return passives.value.find(t => t.id === value && t.type === RuneStageEnum.Passive);
    }

    function getActive(value: number): Item {
        return actives.value.find(t => t.id === value && t.type === RuneStageEnum.Active);
    }

    function getType(value: number): Item {
        return types.value.find(t => t.id === value && t.type === RuneStageEnum.Type);
    }

    function getRegen(value: number): Item {
        return regens.value.find(t => t.id === value && t.type === RuneStageEnum.Color);
    }
    
    async function getData (axios: NuxtAxiosInstance) {
        if (!cachedData && !isFetching) {
            isFetching = true;
            const response: AxiosResponse<Item[]> = await axios.get('/api/items/');
            cachedData = response.data;
            isFetching = false;
        }
        if(cachedData?.length > 0){
            types.value = cachedData.filter(x => x.type === RuneStageEnum.Type);
            regens.value = cachedData.filter(x => x.type === RuneStageEnum.Color);
            passives.value = cachedData.filter(x => x.type === RuneStageEnum.Passive);
            actives.value = cachedData.filter(x => x.type === RuneStageEnum.Active);
        }
    }
    
    return {
        types,
        regens,
        passives,
        actives,
        getData,
        imageBasePath,
        runeGoldCost,
        randomGoldCost,
        salvagedGoldCost,
        getType,
        getPassive,
        getActive,
        getRegen
    }
}
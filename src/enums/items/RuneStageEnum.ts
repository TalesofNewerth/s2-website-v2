export enum RuneStageEnum
{
  Type = 0,
  Color = 1,
  Passive = 2,
  Active = 3
}
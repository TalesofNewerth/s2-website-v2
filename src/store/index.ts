import { NuxtAxiosInstance } from '@nuxtjs/axios'
import jwt from "jsonwebtoken";
import EventSourcePolyfill from "eventsource";

export interface Token {
    email: string;
    username: string;
    accountId: string;
}

export interface AuthState {
    status?: string;
    authToken: string | null;
    refreshToken: string;
    user?: any;
    eventSource?: EventSource | null;
}

export const state = (): AuthState => ({
    status: '',
    authToken: '',
    refreshToken: '',
    user: {},
    eventSource: null,
})

export const mutations = {
    auth_request(state: AuthState) {
        state.status = 'loading';
    },
    auth_success(state: AuthState, newState: AuthState) {
        state.status = 'success';
        state.authToken = newState.authToken;
        state.refreshToken = newState.refreshToken;
        state.user = newState.user;
    },
    auth_error(state: AuthState) {
        state.status = 'error';
    },
    logout(state: AuthState) {
        state.status = '';
        state.authToken = '';
        state.user = {};
    },
    set_event_source(state: AuthState, eventSource: EventSource) {
        console.log("setting event source", eventSource);
        state.eventSource = eventSource;
    },
}

export const actions = {
    auth_request({ commit }) {
        commit('auth_request');
    },
    auth_success({ commit }, newState: AuthState) {
        this.$cookies.set('auth', newState);

        commit('auth_success', <AuthState>{ authToken: newState.authToken, refreshToken: newState.refreshToken, user: newState.user });
    },
    auth_error({ commit }) {
        commit('auth_error');
    },
    login({ commit }, newState: AuthState) {
        return new Promise<void>((resolve) => {
            commit('auth_success', <AuthState>{ authToken: newState.authToken, refreshToken: newState.refreshToken, user: newState.user });
            resolve();
        });
    },
    logout({ commit }) {
        return new Promise<void>((resolve) => {
            this.$cookies.remove('auth');

            commit('logout');
            resolve();
        });
    },
    restoreAuthState({ commit }) {
        const auth = this.$cookies.get('auth');

        if (!auth)
            return;

        const authToken = auth.authToken;
        const refreshToken = auth.refreshToken;

        if (authToken) {
            commit('auth_request');

            const decodedToken: Token | any = jwt.decode(authToken);
            const user = {
                email: decodedToken.email || '',
                username: decodedToken.username || '',
                accountId: decodedToken.accountId || '',
            };

            commit('auth_success', <AuthState>{ authToken: authToken, refreshToken: refreshToken, user: user });
        }
    },
    initializeEventSource({ commit, dispatch }) {
        const auth = this.$cookies.get('auth');

        if (!auth || !auth.authToken) {
            return;
        }

        const eventSource = new EventSourcePolyfill('/api/events/updates?jwt=' + auth.authToken);
        commit('set_event_source', eventSource);
    },
    async nuxtServerInit({ dispatch }) {
        await dispatch('restoreAuthState');
    }
}

export const getters = {
    isLoggedIn: (state: AuthState): boolean => !!state.authToken,
    authStatus: (state: AuthState): string => state.status,
    user: (state: AuthState): Token | any => state.user,
    authToken: (state: AuthState): string => state.authToken,
    refreshToken: (state: AuthState): string => state.refreshToken,
    tokenExpiration: (state: AuthState): number => {
        if (!state.authToken)
            return null;

        const decodedToken: Token | any = jwt.decode(state.authToken);
        return decodedToken.exp;
    }
}
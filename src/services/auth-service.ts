import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { BaseService } from './base-service';
import { Store } from 'vuex';
import { Token, AuthState } from '@/src/store';
import jwt from "jsonwebtoken";

export interface CheckExistsResponse {
    usernameExists: boolean
}
export interface RegisterData {
    username: string,
    email: string,
    password: string
}

export interface RegisterResponse {
    message: string
}

export interface LoginData {
    email: string,
    password: string,
    rememberMe: boolean
}

export interface LoginResponse {
    message: string,
    token: string,
    refreshToken: string
}

export interface RefreshTokenResponse {
    token: string,
    refreshToken: string
}

export interface ResetPasswordData {
    email: string,
    password: string,
    token: string
}

export class AuthService extends BaseService {
    private readonly API_BASE_URL = 'api/authentication';
    private axios: NuxtAxiosInstance;
    private store: Store<AuthState>;

    public get refreshToken(): string {
        return this.store.getters.refreshToken;
    }

    public get authToken(): string {
        return this.store.getters.authToken;
    }

    constructor(axios: NuxtAxiosInstance, store: Store<AuthState>) {
        super();

        this.axios = axios
        this.store = store;

        if (process.client) {
            this.axios.interceptors.response.use(undefined, async error => {
                // Check if it's a request to the login API
                if (error.config &&
                    error.config.url === 'api/authentication/login' ||
                    error.config.url === 'api/authentication/refresh-token') {

                    if (this.store.getters.isLoggedIn)
                        this.logout();

                    return Promise.reject(error);
                }

                if (error.config && error.response && error.response.status === 401 && !error.config._retry) {
                    error._retry = true;

                    // Ignore interceptor if we're refreshing the token
                    if (error.config.headers['X-Ignore-Interceptor']) {
                        return Promise.reject(error);
                    }

                    try {
                        // If the error was Unauthorized (401), try to refresh the token
                        await this.refreshTokenAsync();

                        // Retry the original request
                        return this.axios.request(error.config);
                    } catch (refreshError) {
                        // If the token refresh failed, logout and reject the original request
                        this.logout();
                        return Promise.reject(error);
                    }
                }

                return Promise.reject(error);
            });
        }
    }

    public async register(data: RegisterData): Promise<RegisterResponse> {
        const url = `${this.API_BASE_URL}/register`;
        const response = await this.axios.post<RegisterResponse>(url, data);

        return response.data
    }

    public async login(data: LoginData): Promise<LoginResponse> {
        try {
            const url = `${this.API_BASE_URL}/login`;
            const response = await this.axios.post<LoginResponse>(url, data);

            // Check if the response was successful and a token was returned
            if (response.status === 200 && response.data.token) {
                // Store the token and the refresh token in localStorage or sessionStorage depending on the rememberMe option
                // Decode the JWT to access user information
                const decodedToken: Token | any = jwt.decode(response.data.token);
                const user = {
                    email: decodedToken.email || '',
                    username: decodedToken.username || ''
                };

                await this.store.dispatch('auth_success', {
                    authToken: response.data.token,
                    refreshToken: response.data.refreshToken,
                    user: user,
                });
            }

            return response.data
        }
        catch (error) {
            return Promise.reject(error);
        }
    }

    public async logout() {
        await this.store.dispatch('logout');
    }


    public async checkExists(username: string): Promise<CheckExistsResponse> {
        const url = `${this.API_BASE_URL}/check-exists`;
        const response = await this.axios.post<CheckExistsResponse>(url, { username: username });

        return response.data;
    }

    public async checkEmailExists(email: string): Promise<boolean> {
        const url = `${this.API_BASE_URL}/verify-email`;
        const response = await this.axios.post<boolean>(url, { email: email });

        return response.data;
    }

    public async resendVerificationEmail(email: string) {
        try {
            const url = `${this.API_BASE_URL}/resend-verification`;
            const response = await this.axios.post(url, { email: email });
            return response.data;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public async sendRecoveryEmail(email: string): Promise<string> {
        try {
            const url = `${this.API_BASE_URL}/forgot-password`;
            const response = await this.axios.post(url, { email: email });
            return response.data;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public async resetPassword(data: ResetPasswordData): Promise<string> {
        try {
            const url = `${this.API_BASE_URL}/reset-password`;
            const response = await this.axios.post(url, data);
            return response.data;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public async refreshTokenAsync(): Promise<RefreshTokenResponse> {
        if (!this.refreshToken) {
            return Promise.reject();
        }

        try {
            const url = `${this.API_BASE_URL}/refresh-token`;
            const data = { refreshToken: this.refreshToken };

            const response = await this.axios.post<RefreshTokenResponse>(url, data);
            // If the refresh token is still valid, update the JWT and refresh token in the storage
            if (response.status === 200 && response.data.token) {
                const decodedToken: Token | any = jwt.decode(response.data.token);
                const user = {
                    email: decodedToken.email || '',
                    username: decodedToken.username || '',
                    accountId: decodedToken.accountId || ''
                };

                await this.store.dispatch('auth_success', <AuthState>{
                    authToken: response.data.token,
                    refreshToken: response.data.refreshToken,
                    user: user,
                });

                return Promise.resolve(response.data);
            }
        } catch (error) {

            this.handleError(error);
            return Promise.reject(error);
        }

        return Promise.reject();
    }

    public isTokenExpired(): boolean {
        if (!this.store.getters.tokenExpiration)
            return true;

        const decodedToken: Token | any = jwt.decode(this.store.getters.authToken);
        const expirationDate = decodedToken.exp * 1000;

        return Date.now() >= expirationDate
    }
}

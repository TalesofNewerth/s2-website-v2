import { EventBus } from '../plugins/event-bus';

export class BaseService {
    baseUrl = "/";

    constructor() {
    }

    public handleError(err: any): Promise<void> {
        return new Promise(() => {
            EventBus.$emit('show-dialog', { title: 'Error', text: err.message || 'An error occurred' });
        });
    }
}
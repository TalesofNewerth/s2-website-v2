import Vue from 'vue'
import { AuthService } from '@/services/auth-service'

declare module 'vue/types/vue' {
  interface Vue {
    $authService: AuthService
  }
}

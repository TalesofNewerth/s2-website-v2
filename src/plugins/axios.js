export default function ({ $axios, store }) {
    $axios.onRequest((config) => {
      if (store.state.authToken) {
        config.headers["Authorization"] =
          "Bearer " + store.state.authToken;
      }
    });
}

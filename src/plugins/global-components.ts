import Vue from 'vue';
import Divider from '@/components/utility/divider-component.vue';
import SnackbarComponent from '@/components/utility/snackbar-component.vue';

Vue.component('Divider', Divider);
Vue.component('snackbar-component', SnackbarComponent);

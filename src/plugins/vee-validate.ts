import Vue from 'vue';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import { extend } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';

extend('password-confirmation', {
  params: ['target'],
  validate(value, { target }: { target: string }) {
    return value === target;
  },
  message: 'Password confirmation does not match'
});

extend('required', required);
extend('email', email);


extend('valid-password', {
  validate(value) {
    return /^(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z0-9])[A-Za-z\d\s\S]{8,}$/.test(value);
  },
  message: 'Password must contain at least one special character, one number, one uppercase letter, and be 8 characters minimum'
});

export default ({ app }, inject) => {
  extend('username-exists', {
    async validate(value) {
      if(!value) {
        return false;
      }
      const response = await app.$authService.checkExists(value);
      return !response.usernameExists
    },
    message: 'The username already exists. Please choose another one.'
  });

  extend('email-exists', {
    async validate(value) {
      if(!value) {
        return false;
      }
      return !await app.$authService.checkEmailExists(value);
    },
    message: 'This email already exists. Please choose another one.'
  });
};

extend('valid-username', {
  validate(value) {
    return /^[a-zA-Z0-9-_]{3,25}$/.test(value);
  },
  message: 'The username cannot contain any special characters.'
});

Vue.component('validation-provider', ValidationProvider);
Vue.component('validation-observer', ValidationObserver);


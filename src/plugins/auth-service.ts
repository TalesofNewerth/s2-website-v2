import Vue from 'vue';
import { Plugin } from '@nuxt/types';
import { AuthService } from '@/services/auth-service';

const authService: Plugin = (context, inject) => {
    const service = new AuthService(context.$axios, context.store);

    inject('authService', service);
};

// const eventSource: Plugin = (context, inject) => {
//     const eventSource = new EventSource(`/api/events/updates?jwt=${context.store.getters.authToken}`);


//     inject('eventSource', eventSource);
// };

export default authService;
// export default eventSource;

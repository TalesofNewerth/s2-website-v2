import {RuneStageEnum} from "@/enums/items/RuneStageEnum";

export class Item {
  id: number;
  type?: RuneStageEnum;
  text: string;
  status: string;
  image: string;
  affixImage: string;
  cost: number;
  description: string;
}
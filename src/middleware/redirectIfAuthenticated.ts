import { Middleware } from "@nuxt/types";

const redirectIfAuthenticated: Middleware = (context) => {
    const { redirect, store } = context;
    // const auth = $cookies.get('auth');
    if (!store.getters['authToken'])
        store.dispatch('restoreAuthState');
        
    if (store.getters['isLoggedIn']) {
      return redirect('/');
    }
  };
  
  export default redirectIfAuthenticated;
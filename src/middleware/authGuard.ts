import { Middleware } from '@nuxt/types';

const authGuard: Middleware = async (context) => {
    const { redirect, store } = context;
    if (!store.getters['isLoggedIn']) {
        return redirect('/login');
    }
};

export default authGuard;
# s2website

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

```
s2-website-v2
├─ 📁public
│  └─ 📄index.html
├─ 📁src
│  ├─ 📁components
│  │  ├─ 📁items
│  │  │  ├─ 📄active-items.vue
│  │  │  ├─ 📄item-storage.vue
│  │  │  ├─ 📄items.ts
│  │  │  ├─ 📄rune-builder.vue
│  │  │  └─ 📄rune.vue
│  │  ├─ 📁nav
│  │  │  ├─ 📄nav-bar.vue
│  │  │  └─ 📄nav-menu.vue
│  │  ├─ 📁news
│  │  │  ├─ 📄news-banner.vue
│  │  │  └─ 📄news-item.vue
│  │  ├─ 📁profile
│  │  │  ├─ 📄profile-banner.vue
│  │  │  └─ 📄user-gold.vue
│  │  ├─ 📁stats
│  │  │  ├─ 📄leaderboard.vue
│  │  │  ├─ 📄map.vue
│  │  │  ├─ 📄match-details.vue
│  │  │  ├─ 📄match-stats.vue
│  │  │  ├─ 📄recent-matches.vue
│  │  │  ├─ 📄user-karma.vue
│  │  │  ├─ 📄user-level.vue
│  │  │  └─ 📄user-stats.vue
│  │  ├─ 📁units
│  │  │  ├─ 📄unit-preview.vue
│  │  │  └─ 📄units.ts
│  │  ├─ 📁users
│  │  │  └─ 📄user-search.vue
│  │  ├─ 📁utility
│  │  │  ├─ 📄dialog-component.vue
│  │  │  ├─ 📄divider-component.vue
│  │  │  └─ 📄snackbar-component.vue
│  │  ├─ 📄download-control.vue
│  │  ├─ 📄links.vue
│  │  ├─ 📄logo.vue
│  │  ├─ 📄s2-app-bar.vue
│  │  ├─ 📄s2-footer.vue
│  │  ├─ 📄s2-random-background.vue
│  │  ├─ 📄smoke-effect.vue
│  │  └─ 📄splash-screen.vue
│  ├─ 📁icons
│  │  └─ 📄icons.ts
│  ├─ 📁layouts
│  │  ├─ 📄default.vue
│  │  └─ 📄error.vue
│  ├─ 📁middleware
│  │  ├─ 📄authGuard.ts
│  │  └─ 📄redirectIfAuthenticated.ts
│  ├─ 📁pages
│  │  ├─ 📁matches
│  │  │  ├─ 📄index.vue
│  │  │  └─ 📄_id.vue
│  │  ├─ 📁stats
│  │  │  ├─ 📄index.vue
│  │  │  └─ 📄_username.vue
│  │  ├─ 📄account.vue
│  │  ├─ 📄clans.vue
│  │  ├─ 📄download.vue
│  │  ├─ 📄forgot-password.vue
│  │  ├─ 📄forum.vue
│  │  ├─ 📄index.vue
│  │  ├─ 📄leaderboard.vue
│  │  ├─ 📄login.vue
│  │  ├─ 📄news.vue
│  │  ├─ 📄register.vue
│  │  ├─ 📄reset-password.vue
│  │  ├─ 📄servers.vue
│  │  └─ 📄verify-email.vue
│  ├─ 📁plugins
│  │  ├─ 📄auth-service.ts
│  │  ├─ 📄axios.js
│  │  ├─ 📄event-bus.ts
│  │  └─ 📄global-components.ts
│  ├─ 📁services
│  │  ├─ 📄auth-service.ts
│  │  └─ 📄base-service.ts
│  ├─ 📁static
│  │  ├─ 📁img
│  │  │  ├─ 📁backgrounds
│  │  │  │  ├─ 📄1.png
│  │  │  │  ├─ 📄2.png
│  │  │  │  ├─ 📄3.png
│  │  │  │  ├─ 📄4.png
│  │  │  │  ├─ 📄5.png
│  │  │  │  └─ 📄6.png
│  │  │  ├─ 📁icons
│  │  │  │  ├─ 📁2x
│  │  │  │  │  ├─ 📄batteringram.png
│  │  │  │  │  ├─ 📄behemoth.png
│  │  │  │  │  ├─ 📄builder.png
│  │  │  │  │  ├─ 📄chaplain.png
│  │  │  │  │  ├─ 📄conjurer.png
│  │  │  │  │  ├─ 📄hunter.png
│  │  │  │  │  ├─ 📄legionnaire.png
│  │  │  │  │  ├─ 📄marksman.png
│  │  │  │  │  ├─ 📄predator.png
│  │  │  │  │  ├─ 📄savage.png
│  │  │  │  │  ├─ 📄shaman.png
│  │  │  │  │  ├─ 📄shapeshifter.png
│  │  │  │  │  ├─ 📄steambuchet.png
│  │  │  │  │  ├─ 📄summoner.png
│  │  │  │  │  └─ 📄tempest.png
│  │  │  │  ├─ 📁abilities
│  │  │  │  │  ├─ 📁batteringram
│  │  │  │  │  │  ├─ 📄disembark.png
│  │  │  │  │  │  ├─ 📄flames.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  └─ 📄steamboost.png
│  │  │  │  │  ├─ 📁behemoth
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄shockwave.png
│  │  │  │  │  │  ├─ 📄stomp.png
│  │  │  │  │  │  └─ 📄trunk_slam.png
│  │  │  │  │  ├─ 📁builder
│  │  │  │  │  │  ├─ 📄build.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄rifle.png
│  │  │  │  │  │  ├─ 📄scattergun.png
│  │  │  │  │  │  ├─ 📄shieldgenerator.png
│  │  │  │  │  │  ├─ 📄steamturret.png
│  │  │  │  │  │  └─ 📄supplydepot.png
│  │  │  │  │  ├─ 📁chaplain
│  │  │  │  │  │  ├─ 📄groupheal.png
│  │  │  │  │  │  ├─ 📄heal.png
│  │  │  │  │  │  ├─ 📄holy_bolt.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄resurrection.png
│  │  │  │  │  │  ├─ 📄sol_shield.png
│  │  │  │  │  │  └─ 📄willofgod.png
│  │  │  │  │  ├─ 📁conjurer
│  │  │  │  │  │  ├─ 📄build.png
│  │  │  │  │  │  ├─ 📄entangle.png
│  │  │  │  │  │  ├─ 📄manawell.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄spores.png
│  │  │  │  │  │  └─ 📄venus.png
│  │  │  │  │  ├─ 📁devourer
│  │  │  │  │  │  ├─ 📄consume.png
│  │  │  │  │  │  ├─ 📄decay.png
│  │  │  │  │  │  ├─ 📄gut_launcher.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  └─ 📄vomit.png
│  │  │  │  │  ├─ 📁hunter
│  │  │  │  │  │  ├─ 📄frenzy.png
│  │  │  │  │  │  ├─ 📄glide.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄sentry.png
│  │  │  │  │  │  ├─ 📄sleep.png
│  │  │  │  │  │  ├─ 📄venomous.png
│  │  │  │  │  │  └─ 📄wing_spin.png
│  │  │  │  │  ├─ 📁legionnaire
│  │  │  │  │  │  ├─ 📄bash.png
│  │  │  │  │  │  ├─ 📄grenadelauncher.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄reconstitute.png
│  │  │  │  │  │  ├─ 📄riposte.png
│  │  │  │  │  │  ├─ 📄rocketlauncher.png
│  │  │  │  │  │  └─ 📄whirlingblade.png
│  │  │  │  │  ├─ 📁malphas
│  │  │  │  │  │  ├─ 📄combustion.png
│  │  │  │  │  │  ├─ 📄erruption.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  └─ 📄tephra.png
│  │  │  │  │  ├─ 📁marksman
│  │  │  │  │  │  ├─ 📄backstab.png
│  │  │  │  │  │  ├─ 📄crossbow.png
│  │  │  │  │  │  ├─ 📄democharge.png
│  │  │  │  │  │  ├─ 📄electriceye.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄sniperbow.png
│  │  │  │  │  │  └─ 📄stealth.png
│  │  │  │  │  ├─ 📁predator
│  │  │  │  │  │  ├─ 📄blaze.png
│  │  │  │  │  │  ├─ 📄carnivorous.png
│  │  │  │  │  │  ├─ 📄enrage.png
│  │  │  │  │  │  ├─ 📄fear.png
│  │  │  │  │  │  ├─ 📄fireball.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  └─ 📄venomous.png
│  │  │  │  │  ├─ 📁revenant
│  │  │  │  │  │  ├─ 📄atrophy.png
│  │  │  │  │  │  ├─ 📄decay.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄mortification.png
│  │  │  │  │  │  └─ 📄scorch.png
│  │  │  │  │  ├─ 📁savage
│  │  │  │  │  │  ├─ 📄critical_strike.png
│  │  │  │  │  │  ├─ 📄double_swing.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄morale.png
│  │  │  │  │  │  ├─ 📄rage.png
│  │  │  │  │  │  ├─ 📄ravager.png
│  │  │  │  │  │  └─ 📄repeater.png
│  │  │  │  │  ├─ 📁shaman
│  │  │  │  │  │  ├─ 📄entangle.png
│  │  │  │  │  │  ├─ 📄heal.png
│  │  │  │  │  │  ├─ 📄healing_ward.png
│  │  │  │  │  │  ├─ 📄mana_ward.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄mind_warp.png
│  │  │  │  │  │  ├─ 📄reincarnate.png
│  │  │  │  │  │  └─ 📄stormshield.png
│  │  │  │  │  ├─ 📁shapeshifter
│  │  │  │  │  │  ├─ 📄blind.png
│  │  │  │  │  │  ├─ 📄cheetah.png
│  │  │  │  │  │  ├─ 📄lightning.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄polymorph.png
│  │  │  │  │  │  ├─ 📄sacrifice.png
│  │  │  │  │  │  └─ 📄static.png
│  │  │  │  │  ├─ 📁steambuchet
│  │  │  │  │  │  ├─ 📄disembark.png
│  │  │  │  │  │  ├─ 📄firebombardment.png
│  │  │  │  │  │  ├─ 📄steamboost.png
│  │  │  │  │  │  └─ 📄stonebombardment.png
│  │  │  │  │  ├─ 📁summoner
│  │  │  │  │  │  ├─ 📄heal_pet.png
│  │  │  │  │  │  ├─ 📄locusts.png
│  │  │  │  │  │  ├─ 📄melee.png
│  │  │  │  │  │  ├─ 📄summon_bearloth.png
│  │  │  │  │  │  └─ 📄summon_imp.png
│  │  │  │  │  └─ 📁tempest
│  │  │  │  │     ├─ 📄etheral.png
│  │  │  │  │     ├─ 📄meteor_strike.png
│  │  │  │  │     └─ 📄pushback.png
│  │  │  │  ├─ 📁originals
│  │  │  │  │  ├─ 📄hunter-orig-corrected.png
│  │  │  │  │  └─ 📄hunter-orig.png
│  │  │  │  ├─ 📁rune-icons
│  │  │  │  │  ├─ 📁actives
│  │  │  │  │  │  ├─ 📄effected_brain.png
│  │  │  │  │  │  ├─ 📄effected_dagger.png
│  │  │  │  │  │  ├─ 📄effected_feet.png
│  │  │  │  │  │  ├─ 📄effected_heart.png
│  │  │  │  │  │  ├─ 📄effected_lungs.png
│  │  │  │  │  │  └─ 📄effected_shield.png
│  │  │  │  │  ├─ 📁dds
│  │  │  │  │  │  ├─ 📄animal_armadillo.dds
│  │  │  │  │  │  ├─ 📄animal_bear.dds
│  │  │  │  │  │  ├─ 📄animal_beaver.dds
│  │  │  │  │  │  ├─ 📄animal_cheetah.dds
│  │  │  │  │  │  ├─ 📄animal_dolphin.dds
│  │  │  │  │  │  ├─ 📄animal_fox.dds
│  │  │  │  │  │  ├─ 📄animal_lion.dds
│  │  │  │  │  │  ├─ 📄animal_rabbit.dds
│  │  │  │  │  │  ├─ 📄armadillo.dds
│  │  │  │  │  │  ├─ 📄bear.dds
│  │  │  │  │  │  ├─ 📄beaver.dds
│  │  │  │  │  │  ├─ 📄bg_blue.dds
│  │  │  │  │  │  ├─ 📄bg_red.dds
│  │  │  │  │  │  ├─ 📄bg_white.dds
│  │  │  │  │  │  ├─ 📄bg_yellow.dds
│  │  │  │  │  │  ├─ 📄brain.dds
│  │  │  │  │  │  ├─ 📄dagger.dds
│  │  │  │  │  │  ├─ 📄dolphin.dds
│  │  │  │  │  │  ├─ 📄effected_brain.dds
│  │  │  │  │  │  ├─ 📄effected_brain_large.dds
│  │  │  │  │  │  ├─ 📄effected_dagger.dds
│  │  │  │  │  │  ├─ 📄effected_dagger_large.dds
│  │  │  │  │  │  ├─ 📄effected_feet.dds
│  │  │  │  │  │  ├─ 📄effected_feet_large.dds
│  │  │  │  │  │  ├─ 📄effected_heart.dds
│  │  │  │  │  │  ├─ 📄effected_heart_large.dds
│  │  │  │  │  │  ├─ 📄effected_lungs.dds
│  │  │  │  │  │  ├─ 📄effected_lungs_large.dds
│  │  │  │  │  │  ├─ 📄effected_shield.dds
│  │  │  │  │  │  ├─ 📄effected_shield_large.dds
│  │  │  │  │  │  ├─ 📄feet.dds
│  │  │  │  │  │  ├─ 📄heart.dds
│  │  │  │  │  │  ├─ 📄lungs.dds
│  │  │  │  │  │  ├─ 📄object_amulet.dds
│  │  │  │  │  │  ├─ 📄object_jewel.dds
│  │  │  │  │  │  ├─ 📄object_ring.dds
│  │  │  │  │  │  ├─ 📄object_rune.dds
│  │  │  │  │  │  ├─ 📄rabbit.dds
│  │  │  │  │  │  ├─ 📄rune_builder.png
│  │  │  │  │  │  └─ 📄shield.dds
│  │  │  │  │  ├─ 📁passives
│  │  │  │  │  │  ├─ 📄animal_armadillo.png
│  │  │  │  │  │  ├─ 📄animal_bear.png
│  │  │  │  │  │  ├─ 📄animal_beaver.png
│  │  │  │  │  │  ├─ 📄animal_cheetah.png
│  │  │  │  │  │  ├─ 📄animal_dolphin.png
│  │  │  │  │  │  ├─ 📄animal_fox.png
│  │  │  │  │  │  ├─ 📄animal_lion.png
│  │  │  │  │  │  └─ 📄animal_rabbit.png
│  │  │  │  │  ├─ 📁regen
│  │  │  │  │  │  ├─ 📄bg_blue.png
│  │  │  │  │  │  ├─ 📄bg_red.png
│  │  │  │  │  │  ├─ 📄bg_white.png
│  │  │  │  │  │  └─ 📄bg_yellow.png
│  │  │  │  │  ├─ 📁types
│  │  │  │  │  │  ├─ 📄object_amulet.png
│  │  │  │  │  │  ├─ 📄object_jewel.png
│  │  │  │  │  │  ├─ 📄object_ring.png
│  │  │  │  │  │  └─ 📄object_rune.png
│  │  │  │  │  ├─ 📄armadillo.png
│  │  │  │  │  ├─ 📄bear.png
│  │  │  │  │  ├─ 📄beaver.png
│  │  │  │  │  ├─ 📄brain.png
│  │  │  │  │  ├─ 📄dagger.png
│  │  │  │  │  ├─ 📄dolphin.png
│  │  │  │  │  ├─ 📄effected_brain_large.png
│  │  │  │  │  ├─ 📄effected_dagger_large.png
│  │  │  │  │  ├─ 📄effected_feet_large.png
│  │  │  │  │  ├─ 📄effected_heart_large.png
│  │  │  │  │  ├─ 📄effected_lungs_large.png
│  │  │  │  │  ├─ 📄effected_shield_large.png
│  │  │  │  │  ├─ 📄empty.png
│  │  │  │  │  ├─ 📄feet.png
│  │  │  │  │  ├─ 📄heart.png
│  │  │  │  │  ├─ 📄lungs.png
│  │  │  │  │  ├─ 📄rabbit.png
│  │  │  │  │  ├─ 📄rune_builder.png
│  │  │  │  │  ├─ 📄shield.png
│  │  │  │  │  └─ 📄vault_empty.png
│  │  │  │  ├─ 📁stat-icons
│  │  │  │  │  ├─ 📄1_0.jpg
│  │  │  │  │  ├─ 📄1_1.jpg
│  │  │  │  │  ├─ 📄1_2.jpg
│  │  │  │  │  ├─ 📄behemskull_tl.jpg
│  │  │  │  │  ├─ 📄behemskull_tr.jpg
│  │  │  │  │  ├─ 📄build.jpg
│  │  │  │  │  ├─ 📄hours_played.jpg
│  │  │  │  │  ├─ 📄hps_healed.jpg
│  │  │  │  │  ├─ 📄karma_score.jpg
│  │  │  │  │  ├─ 📄kill_death_ratio.jpg
│  │  │  │  │  ├─ 📄match_record.jpg
│  │  │  │  │  ├─ 📄npc_kills.jpg
│  │  │  │  │  ├─ 📄orb_blue.png
│  │  │  │  │  ├─ 📄player_damage.jpg
│  │  │  │  │  ├─ 📄player_experience.jpg
│  │  │  │  │  ├─ 📄player_rank.jpg
│  │  │  │  │  ├─ 📄razed.jpg
│  │  │  │  │  ├─ 📄resurections.jpg
│  │  │  │  │  ├─ 📄souls_spent.jpg
│  │  │  │  │  ├─ 📄total_assists.jpg
│  │  │  │  │  ├─ 📄total_building_damage.jpg
│  │  │  │  │  ├─ 📄total_deaths.jpg
│  │  │  │  │  ├─ 📄total_gold.jpg
│  │  │  │  │  ├─ 📄total_kills.jpg
│  │  │  │  │  └─ 📄total_losses.jpg
│  │  │  │  ├─ 📄batteringram.png
│  │  │  │  ├─ 📄behemoth.png
│  │  │  │  ├─ 📄builder.png
│  │  │  │  ├─ 📄chaplain.png
│  │  │  │  ├─ 📄conjurer.png
│  │  │  │  ├─ 📄devourer.png
│  │  │  │  ├─ 📄hunter.png
│  │  │  │  ├─ 📄legionnaire.png
│  │  │  │  ├─ 📄malphas.png
│  │  │  │  ├─ 📄predator.png
│  │  │  │  ├─ 📄revenant.png
│  │  │  │  ├─ 📄savage.png
│  │  │  │  ├─ 📄scout.png
│  │  │  │  ├─ 📄shaman.png
│  │  │  │  ├─ 📄shapeshifter.png
│  │  │  │  ├─ 📄steambuchet.png
│  │  │  │  ├─ 📄summoner.png
│  │  │  │  └─ 📄tempest.png
│  │  │  ├─ 📁maps
│  │  │  │  ├─ 📄akamrostu.jpg
│  │  │  │  ├─ 📄ancientcities.jpg
│  │  │  │  ├─ 📄ashrock.jpg
│  │  │  │  ├─ 📄autumn.jpg
│  │  │  │  ├─ 📄bloater_Island.jpg
│  │  │  │  ├─ 📄bunker.jpg
│  │  │  │  ├─ 📄crest.jpg
│  │  │  │  ├─ 📄crossroads.jpg
│  │  │  │  ├─ 📄cyclone.jpg
│  │  │  │  ├─ 📄deadlock.jpg
│  │  │  │  ├─ 📄desolation.jpg
│  │  │  │  ├─ 📄duskwood.jpg
│  │  │  │  ├─ 📄eden.jpg
│  │  │  │  ├─ 📄eternity.jpg
│  │  │  │  ├─ 📄hellpeak.jpg
│  │  │  │  ├─ 📄hiddenvillage.jpg
│  │  │  │  ├─ 📄hyren.jpg
│  │  │  │  ├─ 📄kunlunpass.jpg
│  │  │  │  ├─ 📄losthills.jpg
│  │  │  │  ├─ 📄lostvalley.jpg
│  │  │  │  ├─ 📄mirakar.jpg
│  │  │  │  ├─ 📄mirchul.jpg
│  │  │  │  ├─ 📄moonlight.jpg
│  │  │  │  ├─ 📄morning.jpg
│  │  │  │  ├─ 📄serenity.jpg
│  │  │  │  ├─ 📄snowblind.jpg
│  │  │  │  ├─ 📄storm.jpg
│  │  │  │  ├─ 📄taiga.jpg
│  │  │  │  └─ 📄willow.jpg
│  │  │  ├─ 📄battle-background.png
│  │  │  ├─ 📄beast-horde-title.png
│  │  │  ├─ 📄beast.png
│  │  │  ├─ 📄dice.png
│  │  │  ├─ 📄esrb.png
│  │  │  ├─ 📄flag_Beast.png
│  │  │  ├─ 📄flag_Human.png
│  │  │  ├─ 📄gold.png
│  │  │  ├─ 📄human-legion-title.png
│  │  │  ├─ 📄human.png
│  │  │  ├─ 📄hvb-background.png
│  │  │  ├─ 📄official-logo.png
│  │  │  ├─ 📄official-logo2.png
│  │  │  ├─ 📄official-logo3.png
│  │  │  ├─ 📄official-logo4.png
│  │  │  ├─ 📄rune-builder-demo.png
│  │  │  ├─ 📄savage-2.png
│  │  │  ├─ 📄stone-texture.jpg
│  │  │  └─ 📄vault.jpg
│  │  ├─ 📁vid
│  │  │  ├─ 📄hero-blurred.webm
│  │  │  ├─ 📄hero.webm
│  │  │  ├─ 📄s2cinematic_video.mp4
│  │  │  └─ 📄savage2overview.mp4
│  │  ├─ 📄android-chrome-192x192.png
│  │  ├─ 📄android-chrome-512x512.png
│  │  ├─ 📄apple-touch-icon.png
│  │  ├─ 📄favicon-16x16.png
│  │  ├─ 📄favicon-32x32.png
│  │  ├─ 📄favicon.ico
│  │  ├─ 📄favicon2.ico
│  │  └─ 📄site.webmanifest
│  ├─ 📁store
│  │  └─ 📄index.ts
│  ├─ 📁styles
│  │  └─ 📄styles.scss
│  ├─ 📄aos.d.ts
│  ├─ 📄auth-shim.d.ts
│  ├─ 📄shims-tsx.d.ts
│  ├─ 📄shims-vue.d.ts
│  └─ 📄swiper.d.ts
├─ 📄.dockerignore
├─ 📄.gitignore
├─ 📄.gitlab-ci.yml
├─ 📄babel.config.js
├─ 📄Dockerfile
├─ 📄nuxt.config.ts
├─ 📄package-lock.json
├─ 📄package.json
├─ 📄README.md
├─ 📄tsconfig.json
└─ 📄vue.config.js
```
